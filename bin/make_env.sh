#!/usr/bin/env bash

_ENV_LIST="
  HUBOT_SLACK_TOKEN
  HUBOT_HELP_REPLY_IN_PRIVATE
  MONGODB_URL
  DARKSKY_TOKEN
  WOLFRAM_ALPHA_TOKEN
"

for x in $_ENV_LIST;do
  echo 'export '$x=\"\${$x-$(eval "echo \$$x")}\"
done > .env_generated
